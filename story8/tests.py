from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .views import index
from .apps import Story8Config

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.

class AppsConf(TestCase):
    def test_app(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')

class UrlRouting(TestCase):
    def test_url1_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_url1_using_landing_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_ur1l_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class FuncTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FuncTest, self).setUp()
    
    def tearDown(self):
        self.browser.quit()
        super(FuncTest, self).tearDown()

    def test_accordion(self):
        self.browser.get(self.live_server_url)
        accor1 = self.browser.find_element_by_class_name('accor-sec-0')
        acc1 = self.browser.find_element_by_class_name('accor-content0').value_of_css_property('display')
        accor1.click()

        acc2 = self.browser.find_element_by_class_name('accor-content0').value_of_css_property('display')
        #setelah diklik, content accordion exists
        self.assertEqual('block',acc2)
    
    def test_up(self):
        self.browser.get(self.live_server_url)
        holder = self.browser.find_element_by_class_name("accor-sec-2").text
        index = self.browser.find_element_by_class_name("accor-sec-2").value_of_css_property('order')
        holder9 = self.browser.find_element_by_class_name("accor-sec-"+ str(int(index)-1)).text

        accor_test = self.browser.find_element_by_name('testup')
        accor_test.click()
        holder7 =self.browser.find_element_by_class_name("accor-sec-"+ str(int(index)-1)).text
        #mengecek apakah konten dari index sebelumnya berubah setelah diklik
        self.assertNotEqual(holder7, holder9)

    def test_down(self):
        self.browser.get(self.live_server_url)
        holder = self.browser.find_element_by_class_name("accor-sec-2").text
        index = self.browser.find_element_by_class_name("accor-sec-2").value_of_css_property('order')
        holder9 = self.browser.find_element_by_class_name("accor-sec-"+ str(int(index)+1)).text

        accor_test = self.browser.find_element_by_name('testdown')
        accor_test.click()
        holder7 =self.browser.find_element_by_class_name("accor-sec-"+ str(int(index)+1)).text
        #mengecek apakah konten dari index setelahnya berubah setelah diklik
        self.assertNotEqual(holder7, holder9)

    def test_change_theme(self):
        self.browser.get(self.live_server_url)
        tema_awal = self.browser.find_element_by_tag_name('body').value_of_css_property('background-color')
        button = self.browser.find_element_by_class_name('themee')
        button.click()
        tema_akhir = self.browser.find_element_by_tag_name('body').value_of_css_property('background-color')
        #mengecek tema awal sebelum diklik dan sesudah diklik
        self.assertNotEqual(tema_awal,tema_akhir)
    
    def test_change_style_accordion(self):
        self.browser.get(self.live_server_url)
        style_awal = self.browser.find_element_by_class_name('accordion').value_of_css_property('border-radius')
        button = self.browser.find_element_by_class_name('themee')
        button.click()
        style_akhir = self.browser.find_element_by_class_name('accordion').value_of_css_property('border-radius')
        #mengecek shape awal sebelum diklik dan sesudah diklik
        self.assertNotEqual(style_awal,style_akhir)